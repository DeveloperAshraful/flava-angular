import { Component, OnInit } from '@angular/core';

import { faTwitter, faFacebookSquare, faYoutube } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  Twitter = faTwitter; Facebook = faFacebookSquare; Youtube = faYoutube;

  constructor() { }

  ngOnInit(): void {
  }

}
