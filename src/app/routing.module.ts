import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Pages Component
import { HomePage } from './pages/home/home.component';
import { PageTrakingComponent } from './pages/traking/traking.component';
import { PageFeedbackComponent } from './pages/feedback/feedback.component';
import { PageContactComponent } from './pages/contact/contact.component';
import { PageNotfoundComponent } from './pages/notfound/notfound.component';
import { PageFoodComponent } from './pages/food/food.component';
import { PageSingleComponent } from './pages/single/single.component';
import { PageMenuComponent } from './pages/menu/menu.component';
import { PageOfferComponent } from './pages/offer/offer.component';
import { CoursedetailsComponent } from './pages/coursedetails/coursedetails.component';


const routes: Routes = [
  { path: '', component: HomePage,  pathMatch: 'full'},
  { path: 'foods', component: PageFoodComponent },
  { path: 'foods/:food_id', component: PageSingleComponent },
  { path: 'menus', component: PageMenuComponent },
  { path: 'offers', component: PageOfferComponent },
  { path: 'order-tracking', component: PageTrakingComponent},
  { path: 'feedback', component: PageFeedbackComponent },
  { path: 'contact', component: PageContactComponent},
  { path: 'details/:id', component: CoursedetailsComponent },
  { path: '**', component: PageNotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true } )],
  declarations: [],
  exports: [RouterModule]
})

export class RoutingModule { }

