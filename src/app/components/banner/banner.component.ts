import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  title = 'Page Title';
  sub = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores corporis ipsum quibusdam? Beatae ipsum itaque mollitia natus nisi nostrum veniam vitae. ';

  constructor() { }

  ngOnInit(): void {
  }

}
