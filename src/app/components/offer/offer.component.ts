import { Component, OnInit } from '@angular/core';
import {FoodData} from '../food/foodData';
import {Food} from '../food/food';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {

  foods = FoodData;
  selectedFood: Food;

  constructor() { }

  ngOnInit(): void {
  }

  onSelect(food: Food): void{
    this.selectedFood = food;
  }

}
