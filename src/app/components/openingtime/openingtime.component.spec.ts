import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpeningTimeComponent } from './openingtime.component';

describe('OpeningTimeComponent', () => {
  let component: OpeningTimeComponent;
  let fixture: ComponentFixture<OpeningTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpeningTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpeningTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
