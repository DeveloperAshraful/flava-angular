import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RalatedfoodComponent } from './ralatedfood.component';

describe('RalatedfoodComponent', () => {
  let component: RalatedfoodComponent;
  let fixture: ComponentFixture<RalatedfoodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RalatedfoodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RalatedfoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
