import { Component, OnInit } from '@angular/core';
import {Food} from '../food/food';

@Component({
  selector: 'app-ralatedfood',
  templateUrl: './ralatedfood.component.html',
  styleUrls: ['./ralatedfood.component.css']
})
export class RalatedfoodComponent implements OnInit {

  food: Food;

  constructor() { }

  ngOnInit(): void {
  }

}
