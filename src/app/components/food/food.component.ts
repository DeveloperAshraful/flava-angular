import { Component, OnInit } from '@angular/core';
import { FoodService } from './food.service';
import {Food} from './food';


@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {

  foods: Food[];

  constructor(private foodService: FoodService) { }

  ngOnInit(): void {
    this.getFoods();
  }

  getFoods(): void {
    this.foodService.getFoods().subscribe(foods => this.foods = foods);
  }

}
