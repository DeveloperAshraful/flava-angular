import { Food } from './food';

export const FoodData: Food[] = [
  {
    food_id: 1,
    food_title: 'Roadhouse Crispy Chicken',
    food_excerpt: '879 - 2,310 kcal / 3677.7 - 9665 KJ',
    food_des: 'A BBQ Chicken burger with savoury bacon, lettuce, crispy onions, creamy white cheese and our signature BBQ sauce and a crispy chicken patty, all served in a soft brioche bun.',
    food_img: 'assets/images/chicken-royal.png',
    food_category: 'Roadhouse'
  },
  {
    food_id: 2,
    food_title: 'Steakhouse Angus',
    food_excerpt: '1,030 - 2,233 kcal / 4309.5 - 9342.9 KJ',
    food_des: 'A BBQ Chicken burger with savoury bacon, lettuce, crispy onions, creamy white cheese and our signature BBQ sauce and a crispy chicken patty, all served in a soft brioche bun.',
    food_img: 'assets/images/crispy-chicken.png',
    food_category: 'Steakhouse'
  },
  {
    food_id: 3,
    food_title: 'Bacon Double XL',
    food_excerpt: '1,188 - 3,183 kcal / 4970.6 - 13317.7 KJ',
    food_des: 'Medium Drink and Medium Side IncludedA BBQ Chicken burger with savoury bacon, lettuce, crispy onions, creamy white cheese and our signature BBQ sauce and a crispy chicken patty, all served in a soft brioche bun.',
    food_img: 'assets/images/bacon-double.png',
    food_category: 'Bacon'
  },
  {
    food_id: 6,
    food_title: 'Bacon King',
    food_excerpt: '1,347 - 3,406 kcal / 5635.8 - 14250.7 KJ',
    food_des: 'A BBQ Chicken burger with savoury bacon, lettuce, crispy onions, creamy white cheese and our signature BBQ sauce and a crispy chicken patty, all served in a soft brioche bun.',
    food_img: 'assets/images/bacon-king.png',
    food_category: 'Bacon-King'
  },
  {
    food_id: 4,
    food_title: 'Double whopper',
    food_excerpt: '1,164 - 2,701 kcal / 4870.2 - 11301 KJ',
    food_des: 'A BBQ Chicken burger with savoury bacon, lettuce, crispy onions, creamy white cheese and our signature BBQ sauce and a crispy chicken patty, all served in a soft brioche bun.',
    food_img: 'assets/images/double-whopper.png',
    food_category: 'Double-whopper'
  },
  {
    food_id: 5,
    food_title: 'Bacon Double Cheeseburger',
    food_excerpt: '713 - 1,932 kcal / 2983.2 - 8083.5 KJ',
    food_des: 'A BBQ Chicken burger with savoury bacon, lettuce, crispy onions, creamy white cheese and our signature BBQ sauce and a crispy chicken patty, all served in a soft brioche bun.',
    food_img: 'assets/images/double-cheeseburger.png',
    food_category: 'Cheeseburger'
  }

];
