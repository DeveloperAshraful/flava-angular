export interface Food {
  food_id: number;
  food_title: string;
  food_excerpt: string;
  food_des: string;
  food_img: string;
  food_category: string;
}
