import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Food} from './food';
import {FoodData} from './foodData';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor() { }

  getFoods(): Observable<Food[]>{
    return of(FoodData);
  }

  getFood(id: number): Observable<Food>{
    return of(FoodData.find(food => food.food_id === id));
  }



}
