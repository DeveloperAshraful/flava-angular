export interface Slide {
  slide_id: number;
  slide_title: string;
  slide_subtitle: string;
  slide_button: string;
  slide_button_url: string;
  slide_image: string;
}
