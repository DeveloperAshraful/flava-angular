import {Slide} from './slide';

export const SlideData: Slide[] = [
    {
      slide_id: 1,
      slide_title: 'Want A Whopper For Less?',
      slide_subtitle: 'Praesent Commodo Cursus Magna, Vel Scelerisque Nisl Consectetur.',
      slide_button: 'Check Menu',
      slide_button_url: '/menu',
      slide_image: 'assets/images/slider1.jpg'
    },
    {
      slide_id: 2,
      slide_title: 'Want A Whopper For Less? 2',
      slide_subtitle: 'Praesent Commodo Cursus Magna, Vel Scelerisque Nisl Consectetur.',
      slide_button: 'Check offers',
      slide_button_url: '/',
      slide_image: 'assets/images/slider2.jpg'
    },
    {
      slide_id: 3,
      slide_title: 'Want A Whopper For Less? 3',
      slide_subtitle: 'Praesent Commodo Cursus Magna, Vel Scelerisque Nisl Consectetur.',
      slide_button: 'Check offers',
      slide_button_url: '/',
      slide_image: 'assets/images/slider1.jpg'
    }
  ];
