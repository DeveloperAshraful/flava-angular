import { Component, OnInit } from '@angular/core';
import { FoodService } from '../food/food.service';
import { ActivatedRoute } from '@angular/router';
import { Food } from '../food/food';

@Component({
  selector: 'app-foodsingle',
  templateUrl: './foodsingle.component.html',
  styleUrls: ['./foodsingle.component.css']
})
export class FoodsingleComponent implements OnInit {

  food: Food;

  constructor(private foodService: FoodService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getFood();
  }

  getFood(): void {
    const id = +this.route.snapshot.paramMap.get('food_id');
    this.foodService.getFood(id)
      .subscribe(food => this.food = food);
  }

}
