import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodsingleComponent } from './foodsingle.component';

describe('FoodsingleComponent', () => {
  let component: FoodsingleComponent;
  let fixture: ComponentFixture<FoodsingleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodsingleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodsingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
