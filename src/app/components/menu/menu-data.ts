import {Menu} from './menu';

export const MenuData: Menu[] = [
  {
    menu_id: 1,
    menu_title: 'Flame-grilled burgers',
    menu_image: 'assets/images/burger.jpg'
  },
  {
    menu_id: 2,
    menu_title: 'Crispy & tender chicken',
    menu_image: 'assets/images/crispy-tender-chicken.jpg'
  },
  {
    menu_id: 3,
    menu_title: 'Veggie & more',
    menu_image: 'assets/images/veggie-more.jpg'
  }

];
