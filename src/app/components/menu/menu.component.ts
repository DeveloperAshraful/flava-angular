import { Component, OnInit } from '@angular/core';
import { MenuData } from './menu-data';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  menus = MenuData;

  constructor() { }

  ngOnInit(): void {
  }

}
