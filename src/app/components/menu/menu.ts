export interface Menu {
  menu_id: number;
  menu_title: string;
  menu_image: string;
}
