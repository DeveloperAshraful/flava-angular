import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
// Root Component
import { AppComponent } from './app.component';
// App Routing
import { RoutingModule } from './routing.module';
// NG Bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// NG FontAwesomeModule
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// Common partials Component
import { HeaderComponent } from './partials/header/header.component';
import { FooterComponent } from './partials/footer/footer.component';
// Pages Component
import { HomePage } from './pages/home/home.component';
import { PageContactComponent } from './pages/contact/contact.component';
import { PageFeedbackComponent } from './pages/feedback/feedback.component';
import { PageTrakingComponent } from './pages/traking/traking.component';
import { PageMenuComponent } from './pages/menu/menu.component';
import { PageFoodComponent } from './pages/food/food.component';
import { PageSingleComponent } from './pages/single/single.component';
import { PageOfferComponent } from './pages/offer/offer.component';
import {PageNotfoundComponent} from './pages/notfound/notfound.component';

// Components
import { SliderComponent } from './components/slider/slider.component';
import { BannerComponent } from './components/banner/banner.component';
import { PromotionsComponent } from './components/promotions/promotions.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { WelcomeComponent } from "./components/welcome/welcome.component";
import { OpeningTimeComponent } from "./components/openingtime/openingtime.component";
import { WaitingTimeComponent } from "./components/waitingtime/waitingtime.component";
import { CallactionComponent } from './components/callaction/callaction.component';
import { OrdertrackComponent } from './components/ordertrack/ordertrack.component';
import { FoodComponent } from './components/food/food.component';
import { FoodsingleComponent } from './components/foodsingle/foodsingle.component';
import { RalatedfoodComponent } from './components/ralatedfood/ralatedfood.component';
import { MenuComponent } from './components/menu/menu.component';
import { DeliverComponent } from './components/deliver/deliver.component';
import { OfferComponent } from './components/offer/offer.component';
import { CoursedetailsComponent } from './pages/coursedetails/coursedetails.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomePage,
    PageContactComponent,
    PageFeedbackComponent,
    PageFoodComponent,
    PageSingleComponent,
    PageMenuComponent,
    PageOfferComponent,
    PageTrakingComponent,
    PageNotfoundComponent,
    SliderComponent,
    BannerComponent,
    PromotionsComponent,
    FeedbackComponent,
    WelcomeComponent,
    OpeningTimeComponent,
    WaitingTimeComponent,
    CallactionComponent,
    OrdertrackComponent,
    FoodComponent,
    FoodsingleComponent,
    RalatedfoodComponent,
    MenuComponent,
    DeliverComponent,
    OfferComponent,
    CoursedetailsComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    FormsModule,
    NgbModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
