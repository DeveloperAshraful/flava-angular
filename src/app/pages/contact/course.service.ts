import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Course} from './course';
import {CourseData} from './mock-course';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  getCourses(): Observable<Course[]>{
    return of(CourseData);
  }

  getCourse(id): Observable<any>{
    return of(CourseData.find(courses => courses.id === id ));
  }

  constructor() { }
}
