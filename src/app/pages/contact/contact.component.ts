import { Component, OnInit } from '@angular/core';

import { CourseData } from './mock-course';
import {CourseService} from './course.service';

@Component({
  selector: 'app-page-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class PageContactComponent implements OnInit {

  courses = CourseData;

  constructor( private courseService: CourseService) { }

  ngOnInit(): void {
    this.getCourses();
  }

  getCourses(): void{
    this.courseService.getCourses().subscribe(course => this.courses = course);
  }

}
