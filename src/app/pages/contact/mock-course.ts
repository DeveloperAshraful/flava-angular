
import { Course } from './course';

export  const CourseData: Course[] = [
  { id: 1, name: 'Angular' },
  { id: 2, name: 'React' },
  { id: 3, name: 'Vue' },
  { id: 4, name: 'Blade' },
  { id: 5, name: 'Python' },
  { id: 6, name: 'Express' }
];
