import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PageTrakingComponent} from './traking.component';

describe('PageTrakingComponent', () => {
  let component: PageTrakingComponent;
  let fixture: ComponentFixture<PageTrakingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PageTrakingComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTrakingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
