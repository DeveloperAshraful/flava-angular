import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Course} from '../contact/course';
import {CourseService} from '../contact/course.service';

@Component({
  selector: 'app-coursedetails',
  templateUrl: './coursedetails.component.html',
  styleUrls: ['./coursedetails.component.css']
})
export class CoursedetailsComponent implements OnInit {

   course: Course;
   id: any;

  constructor( private courseService: CourseService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    this.courseService.getCourse(parseInt(this.id)).subscribe(course => {
      this.course = course;
    });
    console.log(this.course);
  }

}
