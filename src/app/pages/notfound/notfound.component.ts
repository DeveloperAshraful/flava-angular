import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-notfound',
  templateUrl: './notfound.component.html',
  styleUrls: ['./notfound.component.css']
})
export class PageNotfoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

  }

}
