import { Component, OnInit } from '@angular/core';
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-page-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css'],
  providers: [NgbTabsetConfig]
})
export class PageFoodComponent implements OnInit {

  row = 'row';

  constructor( config: NgbTabsetConfig) {
    config.justify = 'center';
    config.type = 'pills';
  }

  ngOnInit(): void {}


}
